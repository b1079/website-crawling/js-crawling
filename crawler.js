const request = require('request')
const fs = require('fs');
const getDirName = require('path').dirname;

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

class Crawler {
    constructor() {
        this.url = `http://openplaques.org/plaques/`;
        this.site = 1;
        this.res = [];
        this.timer = 0;
        this.saveTimer = 0;
    }


    async getNextSite() {
        this.timer += 1;
        if (this.timer > 10) {
            this.saveTimer += 1;
            await sleep(3000);
            this.timer = 0;
            if (this.saveTimer > 120) {
                this.saveTimer = 0;
                this.save();
            }
        }
        request(this.url + this.site + '.json', (error, res, body) => {
            const json = JSON.parse(body);

            if (json.area && json.area.country && json.area.country.alpha2 === 'de') {
                this.res.push(json);
            }
        });
        this.site += 1;
    }

    save() {
        const filename = "dump" + new Date().getHours() + '-' + new Date().getMinutes() + '-dump-save.json';
        fs.writeFile(__dirname + '/data-dumps/' + filename, JSON.stringify(this.res), (call) => {
            console.error(call);
            console.log("Saved " + new Date().toISOString() + '-dump-save');
        });


    }

}

async function crawl() {
    const crawler = new Crawler();
    for (let i = 1; i < 50000; i++) {
        await crawler.getNextSite();
    }
}

crawl();
